<?php 
require_once 'functions.php';
?>

<!DOCTYPE html>
<html>
<head>
    <title>Student Management</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <style>
        body {
            font-family: Arial, sans-serif;
        }

        h1, h2 {
            text-align: center;
        }

        table {
            width: 70%;
            margin: 0 auto;
            border-collapse: collapse;
        }

        th, td {
            padding: 10px;
            border: 1px solid #ddd;
        }

        th {
            background-color: #f2f2f2;
        }

        th, .table-btn {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: center;
        }

        .delete-btn {
            margin: 0;
            background-color: #FF0000;
            color: white;
            padding: 10px 15px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            justify-content: center;
        }

        h1 {
            color: #333;
        }

        .section {
            margin-bottom: 20px;
        }

        .section-2 {
            margin-bottom: 10px;
        }

        form {
            width: 50%;
            margin: 20px auto;
        }

        p {
            margin: 5px 0;
        }

        input[type="text"],
        select {
            width: 100%;
            padding: 10px;
            margin: 5px 0;
            box-sizing: border-box;
        }

        input[type="radio"] {
            margin-right: 5px;
        }

        input[type="submit"],
        button {
            padding: 10px 20px;
            background-color: #4CAF50;
            color: white;
            border: none;
            cursor: pointer;
            border-radius: 4px;
        }
    </style>
</head>
<body>
    <h1>Student Management</h1>

    <table id="students-table">
        <thead>
            <tr>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Fakultas</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody id="students-list">
            <?php
            $students = getAllStudents();

            if ($students !== null && is_array($students)) {
                foreach ($students as $student) {
                    echo "<tr>";
                    echo "<td>" . $student['name'] . "</td>";
                    echo "<td>" . $student['gender'] . "</td>";
                    echo "<td>" . $student['faculty'] . "</td>";
                    echo "<td><button class='delete-btn' data-id='" . $student['id'] . "'>Delete</button></td>";
                    echo "</tr>";
                }
            } else {
                echo "<tr><td colspan='4'>Tidak ada data mahasiswa</td></tr>";
            }
            ?>
        </tbody>
    </table>

    <h2>Add Student</h2>
    <form id="add-form">
        <input type="hidden" id="next-id" value="<?php echo count($students) + 1; ?>">
        <div class="section">
            <p>Nama: </p>
            <input type="text" id="name" name="name" required>
        </div>
        <div class="section">
            <p>Jenis Kelamin: </p>
            <div>
                <input type="radio" id="male" name="gender" value="Laki-laki" required>
                <label for="male">Laki-laki</label>
            </div>
            <div>
                <input type="radio" id="female" name="gender" value="Perempuan">
                <label for="female">Perempuan</label>
            </div>
        </div>
        <div class="section-2">
            <p>Fakultas: </p>
            <select id="faculty" name="faculty" required>
                <option value="Fakultas Hukum">Fakultas Hukum</option>
                <option value="Fakultas Ekonomi dan Bisnis">Fakultas Ekonomi dan Bisnis</option>
                <option value="Fakultas Ilmu Administrasi">Fakultas Ilmu Administrasi</option>
                <option value="Fakultas Pertanian">Fakultas Pertanian</option>
                <option value="Fakultas Peternakan">Fakultas Peternakan</option>
                <option value="Fakultas Teknik">Fakultas Teknik</option>
                <option value="Fakultas Kedokteran">Fakultas Kedokteran</option>
                <option value="Fakultas Perikanan dan Ilmu Kelautan">Fakultas Perikanan dan Ilmu Kelautan</option>
                <option value="Fakultas Matematika dan Ilmu Pengetahuan Alam">Fakultas Matematika dan Ilmu Pengetahuan Alam</option>
                <option value="Fakultas Teknologi Pertanian">Fakultas Teknologi Pertanian</option>
                <option value="Fakultas Ilmu Sosial dan Ilmu Politik">Fakultas Ilmu Sosial dan Ilmu Politik</option>
                <option value="Fakultas Ilmu Budaya">Fakultas Ilmu Budaya</option>
                <option value="Fakultas Kedokteran Hewan">Fakultas Kedokteran Hewan</option>
                <option value="Fakultas Ilmu Komputer">Fakultas Ilmu Komputer</option>
                <option value="Fakultas Kedokteran Gigi">Fakultas Kedokteran Gigi</option>
                <option value="Fakultas Ilmu Kesehatan">Fakultas Ilmu Kesehatan</option>
                <option value="Fakultas Vokasi">Fakultas Vokasi</option>
            </select>
        </div>
        <button type="submit">Add</button>
    </form>

    <script>
    $(document).ready(function() {
        $('#add-form').on('submit', function(e) {
            e.preventDefault();

            var formData = {
                id: $('#next-id').val(),
                name: $('#name').val(),
                gender: $('input[name="gender"]:checked').val(), 
                faculty: $('#faculty').val()
            };

            $.ajax({
                type: 'POST',
                url: 'add.php',
                data: formData,
                success: function(data) {
                    $('#students-list').append(data);
                    $('#add-form')[0].reset();
                    $('#next-id').val(parseInt($('#next-id').val()) + 1);
                }
            });
        });


        $(document).on('click', '.delete-btn', function() {
            var id = $(this).data('id');

            $.ajax({
                type: 'POST',
                url: 'delete.php',
                data: { id: id },
                success: function(data) {
                    $(`tr[data-id='${id}']`).remove();
                }
            });
        });
    });
    </script>
</body>
</html>