<?php
require_once 'functions.php';

$id = $_POST['id'];
$name = $_POST['name'];
$gender = $_POST['gender'];
$faculty = $_POST['faculty'];

$student = [
    'id' => $id,
    'name' => $name,
    'gender' => $gender,
    'faculty' => $faculty
];

addStudent($student);

echo "<tr data-id='$id'>";
echo "<td>$name</td>";
echo "<td>$gender</td>";
echo "<td>$faculty</td>";
echo "<td><button class='delete-btn' data-id='$id'>Delete</button></td>";
echo "</tr>";
?>
