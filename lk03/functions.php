<?php
function getAllStudents() {
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    if (isset($_SESSION['students'])) {
        return $_SESSION['students'];
    } else {
        return [];
    }
}

function saveStudents($students) {
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    $_SESSION['students'] = $students;
}

function addStudent($student) {
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    $students = getAllStudents();
    $students[] = $student;
    saveStudents($students);
}

function deleteStudentById($id) {
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    $students = getAllStudents();
    foreach ($students as $key => $student) {
        if ($student['id'] == $id) {
            unset($students[$key]);
            break;
        }
    }
    saveStudents($students);
}
?>
