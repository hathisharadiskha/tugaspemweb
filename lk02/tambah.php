<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tambah Data Mahasiswa</title>

    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
        }

        h1 {
            color: #333;
        }

        .section {
            margin-bottom: 30px;
        }

        .section-2 {
            margin-bottom: 10px;
        }

        form {
            width: 50%;
            margin: 20px 0;
        }

        p {
            margin: 5px 0;
        }

        input[type="text"],
        select {
            width: 100%;
            padding: 10px;
            margin: 5px 0;
            box-sizing: border-box;
        }

        input[type="radio"] {
            margin-right: 5px;
        }

        input[type="submit"] {
            background-color: #4CAF50;
            color: white;
            padding: 10px 15px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }
    </style>
</head>
<body>

<?php
session_start();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $nama = $_POST['nama'];
    $jenis_kelamin = $_POST['jenis_kelamin'];
    $fakultas = $_POST['fakultas'];

    $student = ['nama' => $nama, 'jenis_kelamin' => $jenis_kelamin, 'fakultas' => $fakultas];

    $_SESSION['students'][] = $student;

    header('Location: form[225150401111011].php');
    exit;
}
?>

<h1>Tambah Data Mahasiswa</h1>
<form method="post" action="">
    <div class="section">
        <p>Nama: </p>
        <input type="text" name="nama" required>
    </div>

    <div class="section">
        <p>Jenis Kelamin: </p>
        <input type="radio" name="jenis_kelamin" value="Laki-laki" checked> Laki-laki
        <input type="radio" name="jenis_kelamin" value="Perempuan"> Perempuan
    </div>

    <div class="section-2">
        <p>Fakultas: </p>
        <select name="fakultas" required>
            <option value="Fakultas Hukum">Fakultas Hukum</option>
            <option value="Fakultas Ekonomi dan Bisnis">Fakultas Ekonomi dan Bisnis</option>
            <option value="Fakultas Ilmu Administrasi">Fakultas Ilmu Administrasi</option>
            <option value="Fakultas Pertanian">Fakultas Pertanian</option>
            <option value="Fakultas Peternakan">Fakultas Peternakan</option>
            <option value="Fakultas Teknik">Fakultas Teknik</option>
            <option value="Fakultas Kedokteran">Fakultas Kedokteran</option>
            <option value="Fakultas Perikanan dan Ilmu Kelautan">Fakultas Perikanan dan Ilmu Kelautan</option>
            <option value="Fakultas Matematika dan Ilmu Pengetahuan Alam">Fakultas Matematika dan Ilmu Pengetahuan Alam</option>
            <option value="Fakultas Teknologi Pertanian">Fakultas Teknologi Pertanian</option>
            <option value="Fakultas Ilmu Sosial dan Ilmu Politik">Fakultas Ilmu Sosial dan Ilmu Politik</option>
            <option value="Fakultas Ilmu Budaya">Fakultas Ilmu Budaya</option>
            <option value="Fakultas Kedokteran Hewan">Fakultas Kedokteran Hewan</option>
            <option value="Fakultas Ilmu Komputer">Fakultas Ilmu Komputer</option>
            <option value="Fakultas Kedokteran Gigi">Fakultas Kedokteran Gigi</option>
            <option value="Fakultas Ilmu Kesehatan">Fakultas Ilmu Kesehatan</option>
            <option value="Fakultas Vokasi">Fakultas Vokasi</option>
        </select><br>
    </div>
    <input type="submit" value="Tambah">
</form>

</body>
</html>
