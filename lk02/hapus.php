<?php
session_start();

if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['index'])) {
    $index = $_GET['index'];

    if (isset($_SESSION['students'][$index])) {
        unset($_SESSION['students'][$index]);
    }

    header('Location: form[225150401111011].php');
    exit;
}
?>
