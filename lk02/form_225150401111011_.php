<?php
session_start();

$students = isset($_SESSION['students']) ? $_SESSION['students'] : [];
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daftar Mahasiswa</title>

    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
        }

        h1 {
            color: #333;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }

        th, .data {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }

        th, .table-btn {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: center;
        }

        .delete {
            background-color: #00B4D8;
            color: white;
            padding: 10px 15px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            justify-content: center;
        }

        th {
            background-color: #f2f2f2;
            text-align: center;
        }

        p {
            color: #888;
        }

        .button-container {
            display: flex;
            gap: 10px;
            margin-top: 20px;
        }

        .out {
            background-color: #FF0000;
            color: white;
            padding: 10px 15px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        button {
            background-color: #4CAF50;
            color: white;
            padding: 10px 15px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            justify-content: center;
        }
</style>

</head>
<body>
    <h1>Daftar Mahasiswa</h1>

    <?php
    if (!empty($students)) {
        echo "<table>
                <tr>
                    <th>Nama</th>
                    <th>Jenis Kelamin</th>
                    <th>Fakultas</th>
                    <th>Aksi</th>
                </tr>";

        foreach ($students as $index => $student) {
            echo "<tr>
                    <td class='data'>{$student['nama']}</td>
                    <td class='data'>{$student['jenis_kelamin']}</td>
                    <td class='data'>{$student['fakultas']}</td>
                    <td class='table-btn'>
                        <button class='delete' onclick='hapusData({$index})'>Hapus</button>
                    </td>
                </tr>";
        }

        echo "</table>";
    } else {
        echo "<p>Tidak ada data mahasiswa.</p>";
    }
    ?>

    <?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $nama = $_POST['nama'];
        $jenis_kelamin = $_POST['jenis_kelamin'];
        $fakultas = $_POST['fakultas'];

        $student = ['nama' => $nama, 'jenis_kelamin' => $jenis_kelamin, 'fakultas' => $fakultas];

        $_SESSION['students'][] = $student;

        header('Location: form[225150401111011].php');
        exit;
    }
    ?>

    <div class="button-container">
        <button class="add" onclick='tambahData()'>Tambah Data</button>
        <button class="out" onclick='logout()'>Keluar</button>
    </div>


    <script>
        function tambahData() {
            window.location.href = 'tambah.php';
        }

        function hapusData(index) {
            if (confirm('Apakah Anda yakin ingin menghapus data ini?')) {
                window.location.href = 'hapus.php?index=' + index;
            }
        }

        function logout() {
            window.location.href = 'keluar.php';
        }
    </script>
</body>
</html>
